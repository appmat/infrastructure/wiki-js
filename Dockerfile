ARG WIKI_JS_VERSION=2.5.219
ARG PANDOC_VERSION=2.14.1

FROM pandoc/crossref:${PANDOC_VERSION} as pandoc-crossref

FROM requarks/wiki:${WIKI_JS_VERSION}

# Код ниже полностью заимствован из https://github.com/pandoc/dockerfiles/blob/master/alpine/latex.Dockerfile
USER root

COPY --from=pandoc-crossref \
  /usr/local/bin/pandoc \
  /usr/local/bin/pandoc-citeproc \
  /usr/local/bin/pandoc-crossref \
  /usr/local/bin/

RUN apk --no-cache add \
        freetype \
        fontconfig \
        gnupg \
        gzip \
        librsvg \
        perl \
        tar \
        wget \
        xz

# DANGER: this will vary for different distributions, particularly the
# `linuxmusl` suffix.  Alpine linux is a musl libc based distribution, for other
# "more common" distributions, you likely want just `-linux` suffix rather than
# `-linuxmusl` ------------------------> vvvvvvvvv
ENV PATH="/opt/texlive/texdir/bin/x86_64-linuxmusl:${PATH}"
WORKDIR /root

ADD texlive.profile /root/texlive.profile
COPY install-texlive.sh /root/install-texlive.sh

# Request musl precompiled binary access
RUN echo "binary_x86_64-linuxmusl 1" >> /root/texlive.profile

RUN /root/install-texlive.sh

COPY install-tex-packages.sh /root/install-tex-packages.sh
RUN /root/install-tex-packages.sh

RUN rm -f /root/texlive.profile \
          /root/install-texlive.sh \
          /root/install-tex-packages.sh

# Возвращаем параметры из базового образа https://github.com/Requarks/wiki/blob/dev/dev/build/Dockerfile
WORKDIR /wiki
USER node
