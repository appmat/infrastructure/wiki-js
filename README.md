# wiki-js docker

В этом проекте собираются образы для wiki.js, содержащие pandoc.

За основу взят оригинальный образ [requarks/wiki](https://hub.docker.com/r/requarks/wiki) 
и дополнен по [примеру](https://github.com/pandoc/dockerfiles/blob/master/alpine/latex.Dockerfile) 
из [репозитория pandoc](https://github.com/pandoc/dockerfiles).

Следующие файлы взяты из репозитория pandoc и нужны для более точного ручного версионирования   
* install-tex-packages.sh
* install-texlive.sh
* texlive.profile


Параметры CI, которые можно поменять для быстрой сборки новой версии:

| имя | описание |
| --- | -------- |
| `WIKI_JS_VERSION` | базовая версия wikijs |
| `PANDOC_VERSION` | версия pandoc для установки |
